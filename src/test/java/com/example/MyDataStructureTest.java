package test.java.com.example;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

import main.java.com.example.MyDataStructure;
import org.junit.Assert;
import org.junit.Test;

public class MyDataStructureTest {
	
	@Test
	public void test() {
		MyDataStructure mds = new MyDataStructure(3);

		try {
			mds.put("key1", "value1", 0);
			Thread.sleep(5); // sleep 5ms
			mds.put("key2", "value2", 2000); // time to live: 2000ms
			Thread.sleep(5); // sleep 5ms
			mds.put("key3", "value3", 0);

			Assert.assertTrue(mds.size() == 3);
			Assert.assertTrue(mds.get("key1").equals("value1"));
			Assert.assertTrue(mds.get("key2").equals("value2"));
			Assert.assertTrue(mds.get("key3").equals("value3"));

			Thread.sleep(5); // sleep 5ms
			mds.put("key4", "value4", 0);

			Assert.assertTrue(mds.size() == 3);
			Assert.assertTrue(mds.get("key1") == null);
			Assert.assertTrue(mds.get("key4").equals("value4"));

			Thread.sleep(2500); // sleep 2500ms
			Assert.assertTrue("actual size="+mds.size(), mds.size() == 2);
			Assert.assertTrue(mds.get("key2") == null);
			Assert.assertTrue(mds.get("key3").equals("value3"));
			Assert.assertTrue(mds.get("key4").equals("value4"));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}


	@Test
	public void testConcurrency() throws InterruptedException {
		MyDataStructure mds = new MyDataStructure(3);
		ExecutorService executor = Executors.newFixedThreadPool(10);

		for (int i = 0; i < 100; i++) {
			executor.execute(new Runnable() {
				@Override
				public void run() {
					long time = System.nanoTime();
					System.out.println(time);
					mds.put(String.valueOf(time), time, 100);
				}
			});
		}

		executor.awaitTermination(5, TimeUnit.SECONDS);
		System.out.println(mds.size());
	}

	/*
		This test creates a MyDataStructure with maximum capacity, and updates the most recent key.
		The test checks that the old key is not removed from MyDataStructure.
	 */
	@Test
	public void updateExistingKeyTest() {
		MyDataStructure mds = new MyDataStructure(2);
		mds.put("key1", "value1", 0);
		mds.put("key2", "value2", 0);
		mds.put("key2", "value22", 0);
		Assert.assertTrue(mds.size() == 2);
		Assert.assertTrue(mds.get("key1").equals("value1"));
		Assert.assertTrue(mds.get("key2").equals("value22"));
	}

	/*
		This test creates a full capacity MyDataStructure and adds a new key. We want to check that the key removed
		from MyDataStructure is the key that was last updated, and not last inserted (meaning, updating the value of an
		old key makes the key more recent).
	 */
	@Test
	public void updateOldObjectAndRemoveOldestObjectTest() {
		MyDataStructure mds = new MyDataStructure(2);
		mds.put("key1", "value1", 0);
		mds.put("key2", "value2", 0);
		mds.put("key1", "value3", 0);
		Assert.assertTrue(mds.size() == 2);
		Assert.assertTrue(mds.get("key2").equals("value2"));
		Assert.assertTrue(mds.get("key1").equals("value3"));

		mds.put("key3", "value4", 0);
		Assert.assertTrue(mds.size() == 2);
		Assert.assertTrue(mds.get("key3").equals("value4"));
		Assert.assertTrue(mds.get("key1").equals("value3"));
	}

	/*
		This test checks that 'remove' method in MyDataStructure works properly
	 */
	@Test
	public void removeTest() {
		MyDataStructure mds = new MyDataStructure(1);
		mds.put("key1", "value1", 0);
		Assert.assertTrue(mds.size() == 1);
		Assert.assertTrue(mds.get("key1").equals("value1"));
		mds.remove("key1");
		Assert.assertTrue(mds.size() == 0);
		Assert.assertNull(mds.get("key1"));
	}

	/*
		This test inserts an object to MyDataStructure with given TTL, and then overrides this object with new
		TTL. The test checks that the old TTL has been removed.
	 */
	@Test
	public void updateObjectAndItsTtlTest() {
		MyDataStructure mds = new MyDataStructure(1);
		mds.put("key1", "value1", 1000);
		mds.put("key1", "value11", 0);
		try {
			Thread.sleep(2000);
			Assert.assertTrue(mds.size() == 1);
			Assert.assertTrue(mds.get("key1").equals("value11"));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
