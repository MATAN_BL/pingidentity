package main.java.com.example;

import javafx.util.Pair;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class MyDataStructure {

	private int capacity;
	/**
	 * version is a parameter that "measures" the age of the object inserted. For every put operation it's incremented
	 *  by 1.
	 */
	private int version = 0;
	private Map<String, WrappedObject> dictionary = new ConcurrentHashMap<>();
	/**
	 * insertionOrder holds a queue that gives information about insertion/update history to MyDataStructure.
	 * When MyDataStructure exceeds its capacity, insertionOrder will give
	 */
	List<Pair<String, Integer>> insertionOrder = Collections.synchronizedList(new ArrayList<Pair<String, Integer>>());

	/** WrappedObject is an object that holds every object inserted to MyDataStructure with its version. */
	private class WrappedObject {
		private Object obj;
		private int versionNum;
		public WrappedObject(Object obj, int versionNum) {
			this.obj = obj;
			this.versionNum = versionNum;
		}
		public Object getObj() {
			return obj;
		}
		public int getVersionNum() {
			return versionNum;
		}
	}

	public MyDataStructure(int capacity) {
		this.capacity = capacity;
	}
	
	/**
	 * add value to the data structure. complexity is o(1)
	 * @param key
	 * @param value
	 * @param timeToLive
	 */
	public void put(String key, Object value, int timeToLive) {
		/* The following block is synchronized to avoid a scenario that two distinct threads commit this block
			concurrently. In this undesired case, 'version' member can be updated only once (instead of twice) and/or
			the method 'removeOldestObject' can be called twice when only one element should be removed. */
		synchronized (this) {
			this.version++;
			if (dictionary.size() == capacity && dictionary.get(key) == null) {
				removeOldestObject();
			}
		}
		final int currentVersionNumber = this.version;
		this.dictionary.put(key, new WrappedObject(value, currentVersionNumber));
		this.insertionOrder.add(0, new Pair<>(key, currentVersionNumber));
		if (timeToLive != 0) {
			setTimeout(() -> {
				WrappedObject curValue = dictionary.get(key);
				if (curValue != null && curValue.getVersionNum() == currentVersionNumber) {
					dictionary.remove(key);
				}
			}, timeToLive);
		}
	}

	/**
	 * remove value from the data structure. complexity is o(1)
	 * @param key
	 */
	public void remove(String key) {
		if (dictionary.get(key) != null) {
			dictionary.remove(key);
		}
	}

	/**
	 * get value from the data structure. complexity is o(1)
	 * @param key
	 * @return
	 */
	public Object get(String key) {
		if (dictionary.get(key) != null) {
			return dictionary.get(key).getObj();
		}
		return null;
	}

	/**
	 * get number of keys in the data structure. complexity is o(1)
	 * @return number of keys in the data structure
	 */
	public int size() {
		return dictionary.size();
	}

	private void setTimeout(Runnable runnable, int delay){
		new Thread(() -> {
			try {
				Thread.sleep(delay);
				runnable.run();
			}
			catch (Exception e){
				System.err.println(e);
			}
		}).start();
	}

	private void removeOldestObject() {
		while (true) {
			if (this.insertionOrder == null || this.insertionOrder.size() == 0) break;
			Pair<String, Integer> oldestElement = this.insertionOrder.remove(this.insertionOrder.size() - 1);
			/* if this condition is not satisfied, it means that oldestElement's value has been updated since oldestElement
				was added to insertionOrder */
			if (this.dictionary.get(oldestElement.getKey()).getVersionNum() == oldestElement.getValue()) {
				remove(oldestElement.getKey());
				break;
			}
		}
	}
}
